package Test;
import static org.junit.Assert.*;
import org.junit.Test;
import Animal.Ecureuil;

public class TestEcureuil {

	@Test
	public void testConstrSansParam() {
		Ecureuil e = new Ecureuil();
		assertEquals("Le nombre de points de vie devrait etre egal a 5", 5, e.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e.getStockNourriture());
	}
	
	@Test
	public void testConstrAvecParamPositif() {
		Ecureuil e1 = new Ecureuil(123);
		assertEquals("Le nombre de points de vie devrait etre egal a 123", 123, e1.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e1.getStockNourriture());
	}
	
	@Test
	public void testConstrAvecParamNul() {
		Ecureuil e2 = new Ecureuil(0);
		assertEquals("Le nombre de points de vie devrait etre egal a 0", 0, e2.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e2.getStockNourriture());
	}
	
	@Test
	public void testConstrAvecParamNegatif() {
		Ecureuil e3 = new Ecureuil(-456);
		assertEquals("Le nombre de points de vie devrait etre egal a 0", 0, e3.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e3.getStockNourriture());
	}
	
	@Test
	public void testStockNourriPositive() {
		Ecureuil e4 = new Ecureuil();
		e4.stockerNourriture(123);
		assertEquals("Le nombre de points de vie devrait etre egal a 5", 5, e4.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 123", 123, e4.getStockNourriture());
	}
	
	@Test
	public void testStockNourriNulle() {
		Ecureuil e5 = new Ecureuil();
		e5.stockerNourriture(0);
		assertEquals("Le nombre de points de vie devrait etre egal a 5", 5, e5.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e5.getStockNourriture());
	}
	
	@Test
	public void testStockNourriNegative() {
		Ecureuil e6 = new Ecureuil();
		e6.stockerNourriture(-456);
		assertEquals("Le nombre de points de vie devrait etre egal a 5", 5, e6.getPv());
		assertEquals("Le stock de nourriture stocke par l'ecureuil devrait etre egal a 0", 0, e6.getStockNourriture());
	}
}