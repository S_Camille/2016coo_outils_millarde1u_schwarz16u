package Test;
import static org.junit.Assert.*;
import org.junit.Test;

import Animal.*;

public class TestLoup {
	
	@Test
	public void testConstrSansParametre() {
		Loup l=new Loup();
		assertEquals("Le nombre de point de vie devrait etre egale a 30", 30, l.getPv());
		assertEquals("La nourriture devrait etre a 0",0,l.getStockNourriture());
	}
	
	@Test
	public void testConstructeurAvecParametrePositif(){
		Loup l1=new Loup(12321);
		assertEquals("Le nombre de point de vie devaris egale à 12321",12321,l1.getPv());
		assertEquals("La nourriture devrait etre a 0",0,l1.getStockNourriture());
	}
	
	@Test
	public void testConstructeurAvecParametreNul(){
		Loup l2=new Loup(0);
		assertEquals("Le nombre de point de vie devaris egale à 0",0,l2.getPv());
		assertEquals("La nourriture devrait etre a 0",0,l2.getStockNourriture());
	}
	
	@Test
	public void testConstructeurAvecParametreNegatif(){
		Loup l3=new Loup(-2154);
		assertEquals("Le nombre de point de vie devaris egale à 0",0,l3.getPv());
		assertEquals("La nourriture devrait etre a 0",0,l3.getStockNourriture());
	}
	
	@Test
	public void testStockerNourriturePositif(){
		Loup l4=new Loup();
		l4.stockerNourriture(24);
		assertEquals("La nourriture devrait etre maintenant a 24",24,l4.getStockNourriture());
	}
	
	@Test
	public void testStockerNourritureNul(){
		Loup l5=new Loup();
		l5.stockerNourriture(0);
		assertEquals("La nourriture devrait rester a 0",0,l5.getStockNourriture());
	}
	
	@Test
	public void testStockerNourritureNegatif(){
		Loup l6=new Loup();
		l6.stockerNourriture(-24);
		assertEquals("La nourriture devrait rester a 0",0,l6.getStockNourriture());
	}
	
	@Test
	public void testPasserUnJourAvecNourritureResNul(){
		Loup l7=new Loup();
		l7.stockerNourriture(2);
		boolean res=l7.passerUnjour();
		assertEquals("Le nombre de points de vie devrait etre egale a 30",30,l7.getPv());
		assertEquals("La nourriture devrait rester a 0",0,l7.getStockNourriture());
		assertEquals("Le retour de la mathode devrait etre true",true,res);
		
	}
	
	@Test
	public void testPasserUnJourAvecNourritureResPos(){
		Loup l7=new Loup();
		l7.stockerNourriture(32);
		boolean res=l7.passerUnjour();
		assertEquals("Le nombre de point de vie devaris egale à 30",30,l7.getPv());
		assertEquals("La nourriture devrait rester a 15",15,l7.getStockNourriture());
		assertEquals("Le retour de la mathode devrait etre true",true,res);
		
	}
}
