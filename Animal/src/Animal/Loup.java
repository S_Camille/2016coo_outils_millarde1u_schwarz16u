package Animal;
/**
* modelise un loup
*/

public class Loup implements Animal {
	/**
	 * Attribut entier correspondant � la nourriture stockee par le loup
	 */
	private int viande;
	
	/**
	 * Attribut entier correspondant aux points de vie du loup
	 */
	private int pointDeVie;
	
	/**
	 * Constructeur sans parametre de la classe
	 */
	public Loup() {
		this.pointDeVie = 30;
		this.viande = 0;
	}
	
	/**
	 * Constructeur avec parametre de la classe
	 * @param pv
	 * 		Nombre de points de vie que l'on souhaite ajouter au Loup
	 */
	public Loup(int pv) {
		if (pv < 0){
			this.pointDeVie = 0;
		}
		else{
			this.pointDeVie = pv;
		}
		this.viande = 0;
	}
	
	/**
	 * permet de savoir si le loup est mort
	 * 
	 * @return true si le loup est mort
	 */
	public boolean etreMort() {
		return false;
	}

	/**
	 * fait evoluer le loup d'un jour l'oblige a se nourrir ou a perdre des
	 * points de vie
	 * 
	 * @return true si le loup est vivant a la fin du jour
	 */
	public boolean passerUnjour() {
		boolean b = false;
		if (this.pointDeVie == 0) {
			return b;
		}
		else {
			if (this.viande > 0) {
				this.viande -= 1;
				this.viande = (int) Math.floor(viande/2);
				b = true;
				return b;
			}
			else {
				this.pointDeVie -= 4;
				if (this.pointDeVie <= 0) {
					this.pointDeVie = 0;
					return b;
				}
				else {
					b = true;
					return b;
				}
			}
			
		}
	}

	/**
	 * permet d'ajouter de la nourriture a son stock
	 * 
	 * @param nourriture
	 *       quantite de nourriture stockee
	 */
	public void stockerNourriture(int nourriture) {
		if (nourriture < 0) {
			nourriture = 0;
		}
		this.viande += nourriture;
	}

	/**
	 * retourne les points de vie du loup
	 * @return points de vie du loup
	 */
	public int getPv() {
		return this.pointDeVie;
	}
	
	/**
	 * retourne le stock de nourriture possede par le loup
	 * @return nourriture du loup
	 */
	public int getStockNourriture() {
		return this.viande;
	}
	
	/**
	 * retourne la chaine de caractere correspondant au Loup
	 * @return la chaine de caractere correspondant au Loup
	 */
	public String toString() {
		String s = new String();
		s += "Loup - pv : " + this.getPv() + " viande : " + this.getStockNourriture();
		return s;
	}
}