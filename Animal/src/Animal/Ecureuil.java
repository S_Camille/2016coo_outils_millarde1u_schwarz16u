package Animal;
/**
 * modelise un animal independamment de son type
 */
public class Ecureuil implements Animal{
	
	private int noisette;
	private int pointDeVie;
	
	public Ecureuil(){
		this.pointDeVie = 5;
		this.noisette = 0;
	}
	
	public Ecureuil(int pv){
		if (pv < 0){
			this.pointDeVie = 0;
		}
		else{
			this.pointDeVie = pv;
		}
		this.noisette = 0;
	}
	
	/**
	 * permet de savoir si un animal est mort
	 * 
	 * @return true si l'animal est mort
	 */
	public boolean etreMort(){
		return false;
	}

	/**
	 * fait evoluer l'animal d'un jour l'oblige a se nourrir ou a perdre des
	 * points de vie
	 * 
	 * @return true si l'animal est vivant a la fin du jour
	 */
	public boolean passerUnjour(){
		if (this.pointDeVie == 0){
			return false;
		}
		else{
			if (this.noisette > 0){
				this.noisette-=1;
				if (this.noisette > 0){
					this.noisette-=1;
				}
				return true;
			}
			else{
				if (this.pointDeVie < 3){
					this.pointDeVie = 0;
					return false;
				}
				else{
					this.pointDeVie -=2;
					return true;
				}
			}
		}
	}

	/**
	 * permet d'ajouter de la nourriture a son stock
	 * 
	 * @param nourriture
	 *            quantite de nourriture stockee
	 */
	public void stockerNourriture(int nourriture){
		if (nourriture < 0){
			nourriture = 0;
		}
		this.noisette += nourriture;
	}
	
	/**
	 * retourne les points de vie de l'ecureil
	 * @return points de vie de l'animal
	 */
	
	public int getPv(){
		return this.pointDeVie;
	}
	
	/**
	 * retourne le stocke de nourriture possede par l'animal
	 * @return nourriture de l'animal
	 */
	public int getStockNourriture(){
		return this.noisette;
	}
}